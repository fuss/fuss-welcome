# The Fuss Welcome screen

A fork of mintwelcome (2.4.7) adapted for FUSS. See the original
project on:

https://github.com/linuxmint/mintwelcome

![Screenshot of Fuss Welcome](/screencaptures/welcome_screen_fuss.png?raw=true)

For the localization:
* fist time copy `fusswelcome.pot` into `po/it.po` or similar and change `charset=CHARSET` into `charset=UTF-8`
* when making code modification you need to regenerate the `.pot` file with `./makepot`
* you can apply modifications to the `po` files with `msgmerge -U po/it.po fusswelcome.pot`
* then you can translate all the `po/*.po` files
* rebuilding the package you will get the updated translations
