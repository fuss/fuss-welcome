all: buildmo
buildmo:
	@echo "Building the mo files"
	for file in `ls po`; do \
		lang=`echo $$file | sed 's/\.po$$//'`; \
		install -d usr/share/linuxfuss/locale/$$lang/LC_MESSAGES/; \
		msgfmt -o usr/share/linuxfuss/locale/$$lang/LC_MESSAGES/fusswelcome.mo po/$$file; \
	done

clean:
	rm -rf usr/share/linuxfuss/locale
